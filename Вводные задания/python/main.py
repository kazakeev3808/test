def arm(num):
    num_sun = 0
    n = len(str(num))
    while num: 
        num_sun += (num%10)**n
        num //= 10
    return num_sun

for num in range(1, 32000):
    if num == arm(num):
        print(num)