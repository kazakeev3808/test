def druz(n):
   return sum(i for i in range(1, n) if n % i == 0)
    

for i in range(10000):
    s = druz(i)
    if s > i and druz(s) == i:
        print(i, s)