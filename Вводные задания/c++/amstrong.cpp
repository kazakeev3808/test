#include <iostream>
#include <cmath>

void su(int n){
    int num1 = 0, num2 = 0; 
    double s = 10; 
    int   sum, sum2, st; 
 
    for (int i = 1; i < n; i++) { 
    sum = sum2 = i; 
      while(sum) { 
          sum /= 10; 
          num1++;
      } 
      st = pow(s, num1-1);
      while(sum2) { 
          num2 += pow((double)(sum2 / st), num1); 
          sum2 %= st; 
          st /= 10;
      } 
 
      if(num2 == i) 
        printf("%d\n", i);
      num1 = 0; 
      num2 = 0; 
  } 
}
 
int main() { 
    int n = 32000;
    
    su(n);
    
    return 0;
    
}