#include <iostream>


using namespace std;

void obuchchisla(int n){
    for (int num = 1; num < n; num++) {
        int sum1 = 1;
        
        for (int i = 2; i * i <= num; i++) {
            if (num % i == 0) {
                sum1 += i;

                if (i * i != num)

                    sum1 += num / i;
            }
        }

        if (sum1 > num){
            int num1 = sum1 - 1;
            int sum = 1;
            for (int j = 2; j * j <= num1; j++) {

                if (num1 % j == 0) {

                    sum += j;

                    if (j * j != num1)

                        sum += num1 / j;

                }

            }
            if (sum == num+1) 

                printf("%d, %d\n", num, num1);

        }

    }

}

int main() {
    int n = 10000;
    
    obuchchisla(n);
    
    return 0;
}
