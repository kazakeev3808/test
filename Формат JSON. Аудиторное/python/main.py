import json
def user_info():
    data = {}

    data['people'] = []
    data['people'].append({
        'name': "Scot",
        'city': "London",
        "lastLogin" : "27-02-1905",
        "time" : "4h 26m"

    })
    data['people'].append({
        'name': "Ira",
        'city': "London",
        "lastLogin" : "27-02-1904",
        "time" : "1y 5m 23d 23h 2m"
    })
    data['people'].append({
        'name': "Simon",
        'city': "London",
        "lastLogin" : "27-02-1905",
        "time" : "0h 2m"
    })
    data['people'].append({
        'name': "Roma",
        'city': "London",
        "lastLogin" : "27-04-1905",
        "time" : "4h 30m"
    })
    data['people'].append({
        'name': "Leroy",
        'city': "London",
        "lastLogin" : "27-02-1905",
        "time" : "20h 26m"
    })

    with open("users.json", "w") as write_file:
        json.dump(data, write_file)
    return data

data = user_info()

def user_input():
    with open('users.json') as json_file:
        data = json.load(json_file)
        for p in data['people']:
            s = p['city']
    return p['city']

user_input()