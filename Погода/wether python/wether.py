import json
import requests

appid = "f318ecddd247b2c0f19e3c0175f066c7"

def user_info():
    data = {}

    data['people'] = []
    data['people'].append({
        'name': "Scot",
        'city': "London",
        "lastLogin" : "27-02-1905",
        "time" : "4h 26m"

    })
    data['people'].append({
        'name': "Ira",
        'city': "London",
        "lastLogin" : "27-02-1904",
        "time" : "1y 5m 23d 23h 2m"
    })
    data['people'].append({
        'name': "Simon",
        'city': "London",
        "lastLogin" : "27-02-1905",
        "time" : "0h 2m"
    })
    data['people'].append({
        'name': "Roma",
        'city': "London",
        "lastLogin" : "27-04-1905",
        "time" : "4h 30m"
    })
    data['people'].append({
        'name': "Leroy",
        'city': "London",
        "lastLogin" : "27-02-1905",
        "time" : "20h 26m"
    })

    with open("data_file.json", "w") as write_file:
        json.dump(data, write_file)
    return data

data = user_info()

def user_input():
    with open('data_file.json') as json_file:
        data = json.load(json_file)
        for p in data['people']:
            s = p['city']
    return p['city']

s_city = user_input()

def get_city_id(s_city):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/find",
                 params={'q': s_city, 'type': 'like', 'units': 'metric', 'APPID': appid})
        data = res.json()
        cities = ["{} ({})".format(d['name'], d['sys']['country'])
              for d in data['list']]
        print("city:", cities)
        city_id = data['list'][0]['id']
        print('city_id=', city_id)
    except Exception as e:
        print(e)
    pass
    return city_id

city_id = get_city_id(s_city)

def wether(city_id):
    try:
        res = requests.get("http://api.openweathermap.org/data/2.5/forecast",
                           params={'id': city_id, 'units': 'metric', 'lang': 'ru', 'APPID': appid})
        data = res.json()
        for i in data['list']:
            print( i['dt_txt'], '{0:+3.0f}'.format(i['main']['temp']), i['weather'][0]['main'], i['wind']['speed'])
            with open('wether.json','w') as files:
                json.dump(i, files)
    except Exception as e:
        print(e)
    pass

wether(city_id)