def heapify(args, n, i):

    maximum = i

    if (2*i + 1 < n and args[2*i + 1] > args[i]):
        maximum = 2*i + 1

    if (2*i + 2 < n and args[2*i + 2] > args[maximum]):
        maximum = 2*i + 2

    if (max != i):
        args[i], args[maximum] = args[maximum], args[i]
        heapify(args, n, maximum)

def heapMax(args, n):
   for i in range(n, -1, -1):
        heapify(args, n, i)

   return args

def heapSort(args):
    n = len(args)
    args = heapMax(args, n)
    for i in range(n-1, 0, -1):
        args[i], args[0] = args[0], args[i]
        heapify(args, i, 0)
    
    return args
   
args = [1, 2, 3, 4, 5]
print("Исходная куча: ")
print(args)
print("Отсортированная куча: ")
print(heapSort(args))
