import heapq
import math

def is_complete(H):
    if(math.log(len(H)+1, 2) % 1 ==0):
        print('Полное дерево')
    else:
        print('Не полное дерево')
H = []
print("Введите размер кучи: ")
length = int(input())
for i in range(0, length):
    number = input()
    H.append(number)
heapq.heapify(H)
print(H)
is_complete(H)