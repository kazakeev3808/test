#include <iostream>
#include <cmath>
#include<string>

using namespace std;

int main()
{
  int n;
  cout<<"Правило бинарного дерева: "<<endl;
  cout<<"выполняются неравенства A[i] ≤ A[2i + 1] и A[i] ≤ A[2i + 2] для всех i."<<endl;
  cout<<"Укажите Количество элементов: ";
  cin>>n;
  int *arr=new int[n];  
  cout<<"укажите элементы бинарного дерева в строку через пробел: "; 
  for(int i=0;i<n;i++){
    cin>>arr[i];  
  }
  cout<<"Исходные элементы бинарного дерева: ";
  for(int i=0;i<n;i++){
    if(i<n-1){
      cout<<arr[i]<<",";  
    }
    else{
     cout<<arr[i]<<endl; 
    }
  }
  int temp;
  int ch=0;
  for(int i=0;i<n;i++){
    for(int i=0;i<n;i++){   
      if(2*i+2<n){
        if(arr[i]>=arr[2*i+1] or arr[i]>=arr[2*i+2] ){
          if(arr[2*i+1]<=arr[2*i+2]){
            cout<<arr[i]<<"<->"<<arr[2*i+1]<<endl;
            temp=arr[i];
            arr[i]=arr[2*i+1];
            arr[2*i+1]=temp;
            ch+=1;
          }
          if(arr[2*i+1]>=arr[2*i+2]){
            cout<<arr[i]<<"<->"<<arr[2*i+2]<<endl;
            temp=arr[i];
            arr[i]=arr[2*i+2];
            arr[2*i+2]=temp;
            ch+=1;
          }     
        }
      }
    }
  }
  cout<<"Количество обменов: "<<ch<<endl;
  cout<<"Отсортированный массив по правилам бинарного дерева: ";
  for(int i=0;i<n;i++){
    if(i<n-1){
      cout<<arr[i]<<",";  
    }
    else{
     cout<<arr[i]<<endl; 
    }  
  }
  //cout<<endl<<log2(size+1);
  cout<<"Бинарное дерево по уровням:"<<endl;
  int level=1;
  for(int i=0;i<n;i++){
    if(2*i+1<n){
      if(i==0){
        cout<< "0  level:   "<<"|"<<arr[i]<<"|"<<endl;
        if(2*i+2<n){
          cout<<level<<"  level:   ";
          cout<<arr[i]<<"|"<<arr[2*i+1];
          cout<<","<<arr[2*i+2]<<"|";
        }
      }
      else{
        if(fmod(log2(i+1),1)==0){
          level+=1;
          cout<<endl<<level<<"  level:   ";
        }
        cout<<arr[i]<<"|"<<arr[2*i+1];
        if(arr[2*i+2]==false){
          cout<<"|";
        }
        else{
          cout<<","<<arr[2*i+2]<<"|  ";
        }
      }
    } 
  }
  level=1;
  cout<<endl<<"Бинарное дерево с номерами каждого элемента: "<<endl;
  for(int i=0;i<n;i++){
    if(2*i+1<n){
      if(i==0){
        cout<< "0  level:   "<<"|"<<i<<"|"<<endl;
        if(2*i+2<n){
          cout<<level<<"  level:   ";
          cout<<i<<"|"<<2*i+1;
          cout<<","<<2*i+2<<"|";
        }
      }
      else{
        if(fmod(log2(i+1),1)==0){
          level+=1;
          cout<<endl<<level<<"  level:   ";
        }
        cout<<i<<"|"<<2*i+1;
        if(arr[2*i+2]==false){
          cout<<"|";
        }
        else{
          cout<<","<<2*i+2<<"|  ";
        }
      }
    } 
  }
  if(fmod(log2(n+1),1)==0){
    cout<<endl<<"Данное бинарное дерево является полным";
  }
  else{
    cout<<endl<<"Данное бинарное дерево не является полным";
  } 
}
